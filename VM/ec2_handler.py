import ast
import boto3
import logging
import os

from common import common_functions

LOG_FILE_NAME = 'output.log'

class EC2ResourceHandler:
    """EC2 Resource handler."""

    def __init__(self):
        self.client = boto3.client('ec2')

        logging.basicConfig(filename=LOG_FILE_NAME,
                            level=logging.DEBUG, filemode='w',
                            format='%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        self.logger = logging.getLogger("EC2ResourceHandler")


    # 1. Update the code to search for Amazon Linux AMI ID
    def _get_ami_id(self):
        self.logger.info("Retrieving AMI id")
        images_response = self.client.describe_images(
            Filters=[{'Name': 'architecture',
                      'Values': ['x86_64']},
                     {'Name': 'hypervisor',
                      'Values': ['xen']},
                     {'Name': 'virtualization-type',
                      'Values': ['hvm']},
                     {'Name': 'image-type',
                      'Values': ['machine']},
                     {'Name': 'root-device-type',
                      'Values': ['ebs']}
                     ],
        )
        ami_id = ''
        images = images_response['Images']
        for image in images:
            if 'Name' in image:
                image_name = image['Name']
                # Modify following line to search for Amazon Linux AMI for us-east-1
                if image_name.find("aws-elasticbeanstalk-amzn-2015.03.0.x86_64-nodejs-hvm-201508072126") >= 0:
                    ami_id = image['ImageId']
                    break
        return ami_id
    
    def _get_userdata(self):
        user_data = """
            #!/bin/bash
            yum update -y
            yum install -y httpd24 php56 mysql55-server php56-mysqlnd
            service httpd start
            chkconfig httpd on
            groupadd www
            usermod -a -G www ec2-user
            chown -R root:www /var/www
            chmod 2775 /var/www
            find /var/www -type d -exec chmod 2775 {} +
            find /var/www -type f -exec chmod 0664 {} +
            echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
        """
        return user_data
    
    def _get_security_groups(self):
        security_groups = []
        
        # 2. Get security group id of the 'default' security group
        default_security_group_id = self.get_security_group_id('default')
        # http_security_id = self.get_security_group_id('HttpRequests')
        
        # if http_security_id is None: #check if already exists
        # 3. Create a new security group
        response = self.client.describe_vpcs()
        vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', '')
        response = self.client.create_security_group(GroupName='HttpRequests',
                                         Description='Security group for TCP traffic',
                                         VpcId=vpc_id)
        # 4. Authorize ingress traffic for the group from anywhere to Port 80 for HTTP traffic
        http_security_id = response['GroupId']
        data = self.client.authorize_security_group_ingress(
        GroupId=http_security_id,
        IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 80,
             'ToPort': 80,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}
        ])
        #else:
        security_groups.append(default_security_group_id) #both already exist, return
        security_groups.append(http_security_id)

        return security_groups

    def create(self):
        ami_id = self._get_ami_id()

        if not ami_id:
            print("AMI ID missing..Exiting")
            exit()

        user_data = self._get_userdata()

        security_groups = self._get_security_groups()

        response = self.client.run_instances(
            ImageId=ami_id,
            InstanceType='t2.micro',
            MaxCount=1,
            MinCount=1,
            Monitoring={'Enabled': False},
            UserData=user_data,
            SecurityGroupIds=security_groups
        )
        
        # 5. Parse instance_id from the response
        instance_id = response['Instances'][0]['InstanceId']
        return instance_id


    # 6. Add logic to get information about the created instance
    def get(self, instance_id):
        self.logger.info("Entered get")
        response = self.client.describe_instances(Filters=[
            {
                'Name': 'instance-id',
                'Values': [
                    instance_id,
                ]
            },
        ])
        print(response['Reservations'][0]['Instances'][0]['PublicIpAddress'])
        print(response['Reservations'][0]['Instances'][0]['PublicDnsName'])
        return 

    # 7. Add logic to terminate the created instance
    def delete(self, instance_id):
        self.logger.info("Entered delete")
        self.client.terminate_instances(InstanceIds = [instance_id])
        # # try:
        security_group_id = self.get_security_group_id('HttpRequests')
        self.client.delete_security_group( GroupId = security_group_id)
        print('Security Group Deleted')
        return
    #helper method to find a security group based on groupName
    def get_security_group_id(self, groupName):
        response = self.client.describe_security_groups(Filters=[
            {
                'Name': 'group-name',
                'Values': [
                    groupName,
                ],
            },
        ])
        # did not find security group with groupName
        if response['SecurityGroups'] == []:
            return None
        else: # has a ssecurity group with groupName
            security_groupid = response['SecurityGroups'][0]['GroupId']
            return security_groupid



def main():

    available_cloud_setup = common_functions.get_cloud_setup()
    if 'aws' not in available_cloud_setup:
        print("Cloud setup not found for aws.")
        print("Doing the setup now..")
        os.system("pip install awscli")
        os.system("aws configure")

    ec2_handler = EC2ResourceHandler()

    print("Spinning up EC2 instance")

    instance_id = ec2_handler.create()
    print("EC2 instance provisioning started")

    raw_input("Hit Enter to continue>")
    ec2_handler.get(instance_id)

    raw_input("Hit Enter to continue>")
    ec2_handler.delete(instance_id)
    

if __name__ == '__main__':
    main()